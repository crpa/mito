# mito

#### 项目介绍
蔬菜商城APP
项目框架为VUX+webpack
部分功能使用了H5接口

#### 项目结构
├── build                      // 构建相关    <br>
├── config                     // 配置相关<br>
├── src                        // 源代码<br>
│   ├── api                    // 所有请求<br>
│   ├── assets                 // 主题 字体等静态资源<br>
│   ├── components             // 全局公用组件<br>
│   ├── directive              // 全局指令<br>
│   ├── router                 // 路由<br>
│   ├── store                  // 全局 store管理<br>
│   ├── styles                 // 全局样式<br>
│   ├── utils                  // 全局公用方法<br>
│   ├── vendor                 // 公用vendor<br>
│   ├── views                   // view<br>
│   ├── App.vue                // 入口页面<br>
│   ├── main.js                // 入口 加载组件 初始化等<br>
│   └── permission.js          // 权限管理<br>
├── static                     // 第三方不打包资源<br>
├── .babelrc                   // babel-loader 配置<br>
├── eslintrc.js                // eslint 配置项<br>
├── .gitignore                 // git 忽略项<br>
├── favicon.ico                // favicon图标<br>
├── index.html                 // html模板<br>
└── package.json               // package.json<br>



#### 安装教程

1. npm install i
2. npm run dev


#### 项目打包上线

1.npm run build

