(function(w){

  var ws=null;

  document.addEventListener('plusready',function(){

    plus.navigator.setStatusBarStyle('light');  //设置状态栏 文字颜色 白色
    plus.webview.currentWebview().setStyle({    //设置app 状态栏背景以颜色
      statusbar:{background:'#35495e'},top:0,bottom: 0
    });

    ws=plus.webview.currentWebview();
    // Android处理返回键
    plus.key.addEventListener('backbutton',function(){
      console.log('点击返回');
      back();
    },false);


    console.log("Immersed-UserAgent: "+navigator.userAgent);

  },false);

  var immersed = 0;
  var ms=(/Html5Plus\/.+\s\(.*(Immersed\/(\d+\.?\d*).*)\)/gi).exec(navigator.userAgent);
  if(ms&&ms.length>=3){
    immersed=parseFloat(ms[2]);
  }
  w.immersed=immersed;

  if(!immersed){
    return;
  }
  var t=document.getElementById('header');
//t&&(t.style.paddingTop=immersed+'px',t.style.background='-webkit-linear-gradient(top,rgba(215,75,40,1),rgba(215,75,40,0.8))',t.style.color='#FFF');
  t&&(t.style.paddingTop=immersed+'px',t.style.background='#D74B28',t.style.color='#FFF');
  t=document.getElementById('content');
  t&&(t.style.marginTop=immersed+'px');
  t=document.getElementById('scontent');
  t&&(t.style.marginTop=immersed+'px');
  t=document.getElementById('dcontent');
  t&&(t.style.marginTop=immersed+'px');
  t=document.getElementById('map');
  t&&(t.style.marginTop=immersed+'px');

// 处理返回事件
  w.back=function(hide){
    console.log('处理返回事件');
    if(w.plus){
      ws||(ws=plus.webview.currentWebview());
      if(hide||ws.preate){
        console.log('处理返回事件1');
        ws.hide('auto');
      }else{
        ws.close('auto');
        console.log('处理返回事件2');
      }
    }else if(history.length>1){
      console.log('处理返回事件3');
      history.back();
    }else{
      console.log('处理返回事件3');
      w.close();
    }
  };


})(window);
