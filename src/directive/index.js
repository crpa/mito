import Vue from 'vue'

//自定义全局指令  v-focus='true'  页面加载自动获取焦点
Vue.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时……
  inserted: function (el,{value}) {
    // 聚焦元素
    if (value) {
      el.focus();
    }
  }
});
