import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css'// Progress 进度条样式
import { getToken } from '@/utils/auth'
import user from "./store/modules/user"; // 验权

const whiteList = ['/login','/register']; // 不重定向白名单

const Text = false; //离线测试

router.beforeEach((to, from, next) => {
  NProgress.start();

  if(Text){

    next()

  }else{

    if (store.getters.token) {
      if (to.path === '/login') {
        next({ path: '/' })
        NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
      } else {
        //console.log(store.state.user.userInfo);
        if (store.getters.userInfo == '') {
          store.dispatch('GetInfo').then(res => { // 拉取用户信息
            /*const avatar = res.result.avatar; //用户权限，0普通用户  1管理员
            store.dispatch('GenerateRoutes', { avatar }).then(() => { // 生成可访问的路由表
              router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
              next({ ...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
            })*/
            next()
          }).catch((err) => {
            store.dispatch('FedLogOut').then(() => {
              console.log(err || 'Verification failed, please login again')
              next({ path: '/' })
            })
          })
        } else {
          next()
        }
      }
    } else {
      if (whiteList.indexOf(to.path) !== -1) {
        next()
      } else {
        next('/login')
        NProgress.done()
      }
    }


  }



})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
