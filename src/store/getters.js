const getters = {
  token: state => state.user.token,
  userInfo: state => state.user.userInfo,
  name: state => state.user.name,
  roles: state => state.user.roles,
  content:state => state.user.content,
  newsdata:state => state.appdata.content,
}
export default getters
