import Vue from 'vue'
import Vuex from 'vuex'
import appdata from './modules/data'
import user from './modules/user'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    appdata,
    user
  },
  getters
})

export default store
