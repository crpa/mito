import { newslist } from '@/api/data'
import { getToken, setToken, removeToken } from '@/utils/auth'

const appdata = {
  state: {
    newsdata:'',
    userImg:'', //上传图片
    address:'', //定位地址
    shopCarNum:[], //购物车
  },

  mutations: {
    SET_NEWS:(state, newsdata)=>{
      state.newsdata = newsdata
    },
    SET_USERIMG:(state, val)=>{
      state.userImg = val
    },
    SET_ADDRESS:(state, val)=>{
      state.address = val
    },
    SET_SHOPCARNUM:(state, val)=>{
      state.shopCarNum = val
    },
  },

  actions: {
    // 获取新闻列表
    Newslist({ commit }) {
      return new Promise((resolve, reject) => {
        newslist().then(response => {
          const data = response.data
          commit('SET_NEWS', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
  }
}


export default appdata
