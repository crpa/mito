import { cookie } from 'vux'
import store from '../store'

const TokenKey = 'Admin-Token'

export function getToken() {
  return cookie.get(TokenKey)
}

export function setToken(token) {
  return cookie.set(TokenKey, token)
}

export function removeToken() {
  return cookie.remove(TokenKey)
}


// 产生一个随机数
export function getUid(){
  return Math.floor(Math.random()*100000000+10000000).toString();
}

/* 深度拷贝对象 */
export function depcopy(obj){
  let newobj = {};
  for ( let attr in obj) {
    newobj[attr] = obj[attr];
  }
  return newobj;
}
/* 深度拷贝数组 */
export function arrDeepCopy(source){
  var sourceCopy = [];
  for (var item in source) sourceCopy[item] = typeof source[item] === 'object' ? arrDeepCopy(source[item]) : source[item];
  return sourceCopy;
}

/* 非空 */
export function isEmptyValue(obj) {
  // 本身为空直接返回true
  if (obj == null) return true;
  // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
  if (obj.length > 0)    return false;
  if (obj.length === 0)  return true;
  //最后通过属性长度判断。
  for (let key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }

  return true;
}

/* 数组去重 */     //获取没重复的最右一值放入新数组
export function uniq(array){
  let temp = [];
  let index = [];
  let l = array.length;
  for(let i = 0; i < l; i++) {
    for(let j = i + 1; j < l; j++){
      if (array[i] === array[j]){
        i++;
        j = i;
      }
    }
    temp.push(array[i]);
    index.push(i);
  }
  //console.log(index);
  return temp;
}

/* 自动补充小数点后面两位数，不够补零 */
export function returnFloat(value){
  var value=Math.round(parseFloat(value)*100)/100;
  var xsd=value.toString().split(".");
  if(xsd.length==1){
    value=value.toString()+".00";
    return value;
  }
  if(xsd.length>1){
    if(xsd[1].length<2){
      value=value.toString()+"0";
    }
    return value;
  }
}

/* 加入购物车 存储store */
export function PulsStoreShopCar(nd,n) {
  let pusd = arrDeepCopy(store.state.appdata.shopCarNum);
  let isCop = true;
  if(!isEmptyValue(pusd)){
    console.log(pusd);
    pusd.forEach((val,index,array)=> {
      if(val.id == nd.id){
        val.number += n;
        isCop = false;
      }
    });
    if(isCop){
      pusd.push(nd);
    }
  }else{
    pusd.push(nd);
  }
  store.commit('SET_SHOPCARNUM',pusd)
  //return pusd;
}
