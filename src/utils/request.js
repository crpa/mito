import axios from 'axios'
import Vue from 'vue'
// 创建axios实例
const service = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  retry: 5, //最大请求次数
  timeout: 5000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
  /**
  * code为非1是抛错 可结合自己业务进行修改
  */
    const res = response.data;
    if (res.code !== 1) {
      Vue.$vux.toast.text(res.msg, 'top');
      return Promise.reject('error')
    } else {
      return response.data
    }
  },
  error => {

    //如果连接超时会再次请求，直到超过最多请求次数
    let config = error.config;
    // If config does not exist or the retry option is not set, reject
    if(!config || !config.retry) return Promise.reject(error);

    // 设置请求次数
    config.__retryCount = config.__retryCount || 0;

    // 是否超过最大请求次数
    if(config.__retryCount >= config.retry) {
      //报错
      console.log('错误：' + error)// for debug
      return Promise.reject(error);
    }

    // 请求次数自增
    config.__retryCount += 1;
    console.log('再次请求：'+ config.__retryCount);

    // 设置定时器 超过请求时间后重新请求
    var backoff = new Promise(function(resolve) {
      setTimeout(function() {
        resolve();
      }, config.timeout || 1);
    });

    // Return the promise in which recalls axios to retry the request
    return backoff.then(function() {
      return service.request(config);
    });

  }
)

export default service
