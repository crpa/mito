import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/components/Layout'

export default new Router({
  routes: [
    { path: '/login', component: () => import('@/views/login/index'), hidden: true },
    { path: '/404', component: () => import('@/views/404'), hidden: true },
    {
      path: '/',
      component: Layout,
      redirect:'/home',
      children: [{
        path: '/home',
        component: () => import('@/views/home/home'),
        meta: { title: '主页' }
      },{
        path: '/user',
        component: () => import('@/views/user/user'),
        meta: { title: '个人中心' },
      },{
        path: '/shopIndex',
        component: () => import('@/views/shop/shopIndex'),
        meta: { title: '商城' }
      },{
        path: '/shopCar',
        component: () => import('@/views/shopCar/shopCar'),
        meta: { title: '购物车' }
      },{
        path: '/orderConfirm',
        component: () => import('@/views/order/orderConfirm'),
        meta: { title: '确认订单' }
      },{
        path: '/myOrder',
        component: () => import('@/views/order/myOrder'),
        props: (route) => ({ tabId: route.query.tabId }),
        meta: { title: '我的订单' }
      },{
        path: '/userInfo',
        component: () => import('@/views/user/userInfo'),
        meta: { title: '个人信息' },
        children:[{
          path: 'address',
          component: () => import('@/views/user/address'),
          meta: { title: '地图定位' }
        },{
          path: 'webMap',
          component: () => import('@/views/user/webMap'),
          meta: { title: '地图测试' }
        }]
      },{
        path: '/setting',
        component: () => import('@/views/user/setting'),
        meta: { title: '设置' }
      },{
        path: '/eaditpwd',
        component: () => import('@/views/user/eaditpwd'),
        meta: { title: '修改密码' }
      },{
        path: '/register',
        hidden: true,
        component: () => import('@/views/regis'),
        meta: { title: '注册' }
      }]
    },
    {
      path: '/shopSelect',
      hidden: true,
      component: () => import('@/views/shop/shopSelect'),
      meta: { title: '商城' },
      children:[
        {
          path: 'shopInfo',
          component: () => import('@/views/shop/shopInfo'),
          props: (route) => ({ id: route.query.id }),
          meta: { title: '产品详情' }
        }
      ]
    },
    {
      path: '/page',
      component: Layout,
      children: [{
        path: '/page',
        component: () => import('@/views/domeIcoList'),
        meta: { title: '图标展示' }
      },{
        path: '/news',
        component: () => import('@/views/news/news'),
        meta: { title: '新闻' }
      }]
    },{
      path: '/chart',
      component: Layout,
      children: [{
        path: '/chart',
        component: () => import('@/views/chart/chart'),
        meta: { title: '图表数据' }
      },{
        path: '/bar',
        component: () => import('@/views/chart/bar'),
        meta: { title: '分组柱状图' }
      }]
    },
    { path: '*', redirect: '/404', hidden: true }
  ]
})
