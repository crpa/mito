// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App'

import Vuex from 'vuex'
import store from './store'
import router from './router'
import { ToastPlugin } from 'vux'

import '@/styles/index.less' // global css
import '@/permission' // permission control
import '@/directive'  //自定义全局指令

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(ToastPlugin)

FastClick.attach(document.body)

Vue.config.productionTip = false

//调试工具
import VConsole from 'vconsole/dist/vconsole.min.js' //import vconsole
let vConsole = new VConsole(); // 初始化

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')



window.Promise = require('promise');  //修复三星手机不支持 promise
