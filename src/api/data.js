import request from '@/utils/request'
const qs = require('qs')

/* 注册 */
export function register(d) {
  return request({
    url: '/mito/register',
    method: 'post',
    data: qs.stringify(d)
  })
}

/* 获取产品分类 */
export function getClassification() {
  return request({
    url: '/mito/classification',
    method: 'get',
    params:'',
  })
}

/* 获取产品列表 */
export function getProductList(id) {
  let url = '/mito/productList?id=' + id;
  return request({
    url: url,
    method: 'get',
    params:'',
  })
}



export function newslist() {
  return request({
    url: 'https://www.apiopen.top/journalismApi',
    method: 'post',
    data: qs.stringify({
      q: 'login',
    })
  })
}



//虚拟数据 商品数据
export function domeShopList() {
  const domeList = [{
    id:'1',
    shopId:'1',
    shopName:'叶麻店',
    til:'大白菜香甜可口人见人爱花见不爱',
    money:29.99,
    address:'武汉',
    url:'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2111827976,1534611032&fm=200&gp=0.jpg'
  },{
    id:'2',
    shopId:'1',
    shopName:'叶麻店',
    til:'胡萝卜号称东北人参营养丰富清脆可口',
    money:18.88,
    address:'武汉',
    url:'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2171697428,672549622&fm=26&gp=0.jpg'
  },{
    id:'3',
    shopId:'2',
    shopName:'太和店',
    til:'上好的高碑店神牛牛肉里脊肉',
    money:59.88,
    address:'北京',
    url:'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2771738689,1027890404&fm=200&gp=0.jpg'
  },{
    id:'4',
    shopId:'2',
    shopName:'太和店',
    til:'纯天然压榨花生油纯手工压榨绿色种植花生',
    money:38.99,
    address:'北京',
    url:'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=4223070633,3466598422&fm=26&gp=0.jpg'
  }];
  return domeList;
}
