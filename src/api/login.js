import request from '@/utils/request'
const qs = require('qs')

export function login(username, password) {
  return request({
    url: '/mito/login',
    method: 'post',
    data: qs.stringify({
      userAccount: username,
      passWord: password
    })
  })
}


export function logout(token) {
  return request({
    url: '/mito/getUserInfo',
    method: 'post',
    data: qs.stringify({
      userAccount: token
    })
  })
}

export function getInfo(token) {
  return request({
    url: '/mito/getUserInfo',
    method: 'post',
    data: qs.stringify({
      userAccount: token
    })
  })
}

