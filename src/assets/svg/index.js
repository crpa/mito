/*
*  说明：
 * 将文件夹中svg 复制到 node_modules/vux/src/icons
 *  组件调用<x-icon slot="icon" type="an-megaphone" ></x-icon>
 *  然后设置样式 宽高。
 *  默认自定义的svg没有设置宽高
  *
  * 以下代码仅供预览使用
  * */


/*  浏览器需要打开ActiveXObject控件，仅支持IE和火狐   */
export function domeIcoList(){
  var tbsource = "D:/an/vue/vux/node_modules/vux/src/icons";//本地文件夹路径
  let nerul = '/node_modules/vux/src/icons/';

  var hdfiles = [];

  var objFSO =new ActiveXObject('Scripting.FileSystemObject');

  if(!objFSO.FolderExists(tbsource))

  {

    alert("<"+tbsource+">该文件夹路径不存在，或者路径不能含文件名！");

    objFSO = null;

//return;

  }

  var objFolder = objFSO.GetFolder(tbsource);

  var colFiles = new Enumerator(objFolder.Files);

//var re_inf1 = /\.jpg$/; 验证文件夹文件是否jpg文件

//var re_inf1 = /\[.](jpg|gif|bmg)$/;

  var re_inf1 =/\.(gif|jpg|jpeg|bmp|svg)$/i;

  for (; !colFiles.atEnd(); colFiles.moveNext()) //读取文件夹下文件

  {

    var objFile = colFiles.item();

    if(re_inf1.test(objFile.Name.toLowerCase()))

    {
      hdfiles.push({
        url:nerul+objFile.Name,
        name:objFile.Name
      })
      //hdfiles = hdfiles+"<img src='"+nerul+"/"+objFile.Name+"'>";
    }

  }

  //alert(hdfiles);
  //document.getElementById("pic").innerHTML=hdfiles;
  return hdfiles;

}

