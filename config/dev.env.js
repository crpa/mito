'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"https://easy-mock.com/mock/5b7692768799bc666930a816/example"',  //跨域使用  '/api'     模拟接口 https://easy-mock.com/mock/5b7692768799bc666930a816/example
})
